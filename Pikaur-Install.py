#! /bin/python3
# Justin Vermeer - 2018
# Dependencies are base-devel, pyalpm, git, wget and pacman.
# No AUR deps needed.
import os
import subprocess

if os.getuid() == 0:
    print('Do not run this as root!, make a user and try again...\n')
    print('Closing...\n')
    exit()


def dep_inst():
    x = subprocess.call('sudo pacman -Sy --noconfirm --needed wget pyalpm git >> /dev/null', shell=True)
    if x == 0:
        print('Deps sorted.')
        return 0
    if x != 0:
        print('\n\nIssue installing dependencies')
        print('Returned ' + str(x))
        return 1
        exit()


def inst():
    url = "https://aur.archlinux.org/cgit/aur.git/snapshot/pikaur.tar.gz"
    subprocess.call('wget ' + url, shell=True)
    subprocess.call("tar -xpf pikaur.tar.gz && cd pikaur/ && makepkg -si --noconfirm", shell=True)
    subprocess.call("rm -rf pikaur/ pikaur.tar.gz", shell=True)


if dep_inst() == 0:
    print('Great success')
    inst()
else:
    ping = subprocess.call('ping google.ca > /dev/null', shell=True)
    if ping == 0:
        print('Network alive...\n')
    else:
        print('\n\nConnect to a fucking network...')
        exit()
